Uso de la calculadora Cardiocoop


PASOS PARA MODIFICAR EL % DE INTERÉS DE LAS LINEAS DE CREDITO

en la linea de codigo # 160 podemos encontrar la lista de los posibles tipos de credito con su correspondiente % de interés 
aparecerá de la siguiente manera

        <select id="list" >           <!-- contiene la lista de Tipos de crédito -->
        <option value="2">Avance de nomina</option>
        <option value="0.75">Calamidad</option>
        <option value="0.95">Compra de vehiculo</option>
        <option value="1">Compra de cartera</option>
        <option value="0.95">Compra de moto nueva</option>
        <option value="1.85">Consumo</option>
        <option value="0.8">Crediaportes</option>
        <option value="1">Cuota inicial compra vivienda nueva</option>
        <option value="1">Educativo</option>
        <option value="1.8">Electrodomestico</option>
        <option value="2">Gerencia</option>
        <option value="1.95">Ordinario con codeudor</option>
        <option value="1.95">Ordinario sin codeudor</option>
        <option value="1">Pago impuesto y reforma vivienda</option>
        <option value="1.5">Prima</option>
        <option value="1.6">Turismo y recreación</option>
        <option value="1.85">Convenios y seguros</option>
        </select>
        
   
 
   
Tomemos una opción como ejemplo:
     " < option value="0.95" > Compra de vehiculo < /option .>  "
     
         en este caso nuestra opción es la de "Compra de vehiculo" podemos ver el nombre correspondiente en medio de los dos iconos >  <
         y su % correspondiente es el que encontramos en <option value="0.95">   
         el valor de Value, en este caso "0.95" es el valor del % del credito 
         para cambiar la tasa de interés de cada opción de credito debemos modificar el numero correspondiente
         
         esta modificacion debemos hacerla en el archivo "Calculadora" y finalmente copiar y pegar el codigo completo a la pagina de cardiocoop
         
         
         
         
         
PASOS PARA AÑADIR O QUITAR UNA LINEA DE CREDITO
         
 Para añadir una linea de credito nueva debemos usar el codigo
 <option value="x"> Nombre de la linea </option> donde "X" es el % de interés
 
 la añadimos en la lista anterior
 
 
 PASOS PARA AÑADIR CUOTA ADMINISTRATIVA A UNA LINEA DE CREDITO
 
 en la linea de codigo #33 encontramos
        "let S = [0, 2, 5, 6, 10, 14, 16].includes(G) ? (y*0.00042) : (y*0.00342);"
        
donde los numeros dentro de [ ] hacen referencia a la lista de creditos que encontramos en el codigo #160
es decir


        <option value="2">Avance de nomina</option>                         =0
        <option value="0.75">Calamidad</option>                             =1
        <option value="0.95">Compra de vehiculo</option>                    =2
        <option value="1">Compra de cartera</option>                        =3
        <option value="0.95">Compra de moto nueva</option>                  =4
        <option value="1.85">Consumo</option>                               =5
        <option value="0.8">Crediaportes</option>                           =6
        <option value="1">Cuota inicial compra vivienda nueva</option>      =7
        <option value="1">Educativo</option>                                =8
        <option value="1.8">Electrodomestico</option>                       =9
        <option value="2">Gerencia</option>                                 =10
        <option value="1.95">Ordinario con codeudor</option>                =11
        <option value="1.95">Ordinario sin codeudor</option>                =12
        <option value="1">Pago impuesto y reforma vivienda</option>         =13
        <option value="1.5">Prima</option>                                  =14
        <option value="1.6">Turismo y recreación</option>                   =15
        <option value="1.85">Convenios y seguros</option>                   =16
        </select>
        
        
Para poder añadir o quitar la cuota administrativa en las lineas de credito se debe añadir o quitar el numero correspondiente en
"let S = [0, 2, 5, 6, 10, 14, 16].includes(G) ? (y*0.00042) : (y*0.00342);"